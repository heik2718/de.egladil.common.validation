//=====================================================
// Projekt: de.egladil.persistence.tools
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.common.validation.validators;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.egladil.common.validation.annotations.Honeypot;

/**
 * PasswortValidatorTest
 */
public class HoneypotValidatorTest {

	private static final Logger LOG = LoggerFactory.getLogger(HoneypotValidatorTest.class);

	private class TestObject {

		@Honeypot
		private String value;

		/**
		 * Erzeugt eine Instanz von TestObject
		 */
		public TestObject() {
			super();
		}

		/**
		 * Setzt die Membervariable
		 *
		 * @param value neuer Wert der Membervariablen value
		 */
		public void setValue(final String value) {
			this.value = value;
		}
	}

	@Test
	@DisplayName("passes when value null")
	public void validate1() {
		// Arrange
		final TestObject credentials = new TestObject();
		credentials.setValue(null);

		final ValidatorFactory validatorFactory = Validation.buildDefaultValidatorFactory();
		final Validator validator = validatorFactory.getValidator();

		// Act
		final Set<ConstraintViolation<TestObject>> errors = validator.validate(credentials);

		// Assert
		assertTrue(errors.isEmpty());
	}

	@Test
	@DisplayName("passes when value empty")
	public void validate2() {
		// Arrange
		final TestObject credentials = new TestObject();
		credentials.setValue("");

		final ValidatorFactory validatorFactory = Validation.buildDefaultValidatorFactory();
		final Validator validator = validatorFactory.getValidator();

		// Act
		final Set<ConstraintViolation<TestObject>> errors = validator.validate(credentials);

		// Assert
		assertTrue(errors.isEmpty());
	}

	@Test
	@DisplayName("fails when value blank not empty")
	public void validate3() {
		// Arrange
		final TestObject credentials = new TestObject();
		credentials.setValue("        ");

		final ValidatorFactory validatorFactory = Validation.buildDefaultValidatorFactory();
		final Validator validator = validatorFactory.getValidator();

		// Act
		final Set<ConstraintViolation<TestObject>> errors = validator.validate(credentials);

		// Assert
		assertFalse(errors.isEmpty());
		assertEquals(1, errors.size());

		final ConstraintViolation<TestObject> cv = errors.iterator().next();
		LOG.debug(cv.getMessage());
		assertEquals("value", cv.getPropertyPath().toString());
	}

	@Test
	@DisplayName("passes when value not blank")
	public void validate4() {
		// Arrange
		final TestObject credentials = new TestObject();
		credentials.setValue("x");

		final ValidatorFactory validatorFactory = Validation.buildDefaultValidatorFactory();
		final Validator validator = validatorFactory.getValidator();

		// Act
		final Set<ConstraintViolation<TestObject>> errors = validator.validate(credentials);

		// Assert
		assertFalse(errors.isEmpty());
		assertEquals(1, errors.size());

		final ConstraintViolation<TestObject> cv = errors.iterator().next();
		LOG.debug(cv.getMessage());
		assertEquals("value", cv.getPropertyPath().toString());
	}
}
