//=====================================================
// Projekt: de.egladil.persistence.tools
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.common.validation;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

/**
 * PrettyStringUtilsTest
 */
public class ValidationStringUtilsTest {

	@Test
	@DisplayName("collection null and separationChars null")
	public void collectionToString1() {
		assertEquals("", ValidationStringUtils.collectionToString(null, null));
	}

	@Test
	@DisplayName("collection null and separationChars leer")
	public void collectionToString2() {
		assertEquals("", ValidationStringUtils.collectionToString(null, ""));
	}

	@Test
	@DisplayName("collection ein element and separationChars null")
	public void collectionToString3() {
		final List<String> liste = Arrays.asList(new String[] { "A" });
		assertEquals("A", ValidationStringUtils.collectionToString(liste, null));
	}

	@Test
	@DisplayName("collection ein element and separationChars leer")
	public void collectionToString4() {
		final List<String> liste = Arrays.asList(new String[] { "A" });
		assertEquals("A", ValidationStringUtils.collectionToString(liste, ""));
	}

	@Test
	@DisplayName("collection zwei elemente and separationChars null")
	public void collectionToString5() {
		final List<String> liste = Arrays.asList(new String[] { "A", "B" });
		assertEquals("AB", ValidationStringUtils.collectionToString(liste, null));
	}

	@Test
	@DisplayName("collection zwei elemente and separationChars null")
	public void collectionToString6() {
		final List<String> liste = Arrays.asList(new String[] { "A", "B" });
		assertEquals("AB", ValidationStringUtils.collectionToString(liste, ""));
	}

	@Test
	@DisplayName("collection zwei elemente and separationChars semikolon")
	public void collectionToString7() {
		final List<String> liste = Arrays.asList(new String[] { "A", "B" });
		assertEquals("A;B", ValidationStringUtils.collectionToString(liste, ";"));
	}

	@Test
	public void toStringOhneDubletten() {
		// Arrange
		final String string = "keine gültige E-Mail-Adresse, keine gültige E-Mail-Adresse";
		final String expected = "keine gültige E-Mail-Adresse";

		// Act
		final String actual = ValidationStringUtils.toStringOhneDubletten(string, ",");

		// Assert
		assertEquals(expected, actual);
	}
}
