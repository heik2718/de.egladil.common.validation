//=====================================================
// Projekt: de.egladil.mkm.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.common.validation.json;

import com.fasterxml.jackson.annotation.JsonProperty;

import de.egladil.common.validation.ValidationStringUtils;

/**
 * InvalidProperties.<br>
 * <br>
 * Das Attribut 'name' bestimmt, an welchem Form-Element die 'message' angezeigt wird.
 */
public class InvalidProperty {

	@JsonProperty
	private String name;

	@JsonProperty
	private String message;

	/**
	 * Erzeugt eine Instanz von InvalidProperties
	 */
	public InvalidProperty() {
		// TODO Generierter Code

	}

	/**
	 * Erzeugt eine Instanz von InvalidProperties
	 */
	public InvalidProperty(String fieldName, String message) {
		this.name = fieldName;
		this.message = ValidationStringUtils.toStringOhneDubletten(message, ",");
	}

	String getName() {
		return name;
	}

	String getMessage() {
		return message;
	}
}
