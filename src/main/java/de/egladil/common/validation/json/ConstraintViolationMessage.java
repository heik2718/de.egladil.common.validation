//=====================================================
// Projekt: de.egladil.mkm.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.common.validation.json;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * ConstraintViolationMessage
 */
public class ConstraintViolationMessage {

	@JsonProperty
	private String globalMessage = "Die Eingaben sind nicht korrekt.";

	@JsonProperty
	private String crossValidationMessage = "";

	@JsonProperty
	private List<InvalidProperty> invalidProperties = new ArrayList<>();

	/**
	 * Erzeugt eine Instanz von ConstraintViolationMessage
	 */
	public ConstraintViolationMessage() {
	}

	/**
	 * Erzeugt eine Instanz von ConstraintViolationMessage
	 */
	public ConstraintViolationMessage(String message) {
		this.globalMessage = message;
	}

	/**
	 * Setzt die Membervariable
	 *
	 * @param invalidProperties neuer Wert der Membervariablen invalidProperties
	 */
	public void setInvalidProperties(List<InvalidProperty> invalidProperties) {
		this.invalidProperties = invalidProperties;
	}

	public void addInvalidProperty(InvalidProperty prop) {
		invalidProperties.add(prop);
	}

	public boolean containsMessage(String message) {
		for (InvalidProperty prop : invalidProperties) {
			if (prop.getMessage().equals(message)) {
				return true;
			}
		}
		return false;
	}

	public boolean containsKeyAndMessage(String key, String message) {
		for (InvalidProperty prop : invalidProperties) {
			if (prop.getMessage().equals(message) && prop.getName().equals(key)) {
				return true;
			}
		}
		return false;
	}

	public void setCrossValidationMessage(String crossValidationMessage) {
		this.crossValidationMessage = crossValidationMessage;
	}

	/**
	* @see java.lang.Object#toString()
	*/
	@Override
	public String toString() {
		return "ConstraintViolationMessage [globalMessage=" + globalMessage + ", crossValidationMessage=" + crossValidationMessage
			+ "]";
	}

	@JsonIgnore
	public final String getGlobalMessage() {
		return globalMessage;
	}
}
