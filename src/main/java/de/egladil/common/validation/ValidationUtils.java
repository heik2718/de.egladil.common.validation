//=====================================================
// Projekt: de.egladil.persistence.tools
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.common.validation;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Path;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;

import de.egladil.common.validation.json.ConstraintViolationMessage;
import de.egladil.common.validation.json.InvalidProperty;

/**
 * ValidationUtils
 */
public class ValidationUtils {

	/**
	 * Gibt eine Menge von Namen der invaliden properties aus den ConstraintViolations zurück.
	 *
	 * @param errors
	 * @param properties
	 */

	public <T> Set<String> extractPropertyNames(Set<ConstraintViolation<T>> errors) throws IllegalArgumentException {
		if (errors == null) {
			throw new IllegalArgumentException("errors darf nicht null sein");
		}
		Iterator<ConstraintViolation<T>> iter = errors.iterator();
		Set<String> properties = new HashSet<>();
		while (iter.hasNext()) {
			ConstraintViolation<T> violation = iter.next();
			properties.add(violation.getPropertyPath().toString());
		}
		return properties;
	}

	/**
	 * Schreibt die ConstraintViolation ins log.
	 *
	 * @param errors
	 * @param log
	 */
	public <T> void debugConstraintViolation(Set<ConstraintViolation<T>> errors, Logger log) {
		if (!log.isDebugEnabled()) {
			return;
		}
		Iterator<ConstraintViolation<T>> iter = errors.iterator();
		while (iter.hasNext()) {
			ConstraintViolation<T> cv = iter.next();
			log.debug(cv.toString());
		}
	}

	/**
	 * Validation-Messages in verarbeitbarer Form extrahieren.
	 *
	 * @param errors
	 * @param bean
	 * @return
	 * @throws IllegalArgumentException
	 */
	public <T> Map<String, String> extractPropertiesAndMessages(Set<ConstraintViolation<T>> errors, Class<T> clazz)
		throws IllegalArgumentException {
		if (errors == null) {
			throw new IllegalArgumentException("errors darf nicht null sein");
		}
		if (clazz == null) {
			throw new IllegalArgumentException("clazz darf nicht null sein");
		}
		Set<String> fieldNames = fieldNames(clazz);
		Map<String, String> result = new HashMap<>();
		Iterator<ConstraintViolation<T>> iter = errors.iterator();
		while (iter.hasNext()) {
			ConstraintViolation<T> cv = iter.next();
			Path path = cv.getPropertyPath();
			String propName = path.toString();
			if (fieldNames.contains(propName)) {
				String message = result.get(propName);
				if (message == null) {
					message = cv.getMessage();
				} else {
					message += ", " + cv.getMessage();
				}
				result.put(propName, message);
			}
		}
		return result;
	}

	public <T> ConstraintViolationMessage toConstraintViolationMessage(Set<ConstraintViolation<T>> errors, Class<T> clazz) {
		Map<String, String> messages = this.extractPropertiesAndMessages(errors, clazz);
		ConstraintViolationMessage result = new ConstraintViolationMessage();
		if (!messages.isEmpty()) {
			for (String key : messages.keySet()) {
				final String message = messages.get(key);
				if (!result.containsKeyAndMessage(key, message)) {
					InvalidProperty prop = new InvalidProperty(key, message);
					result.addInvalidProperty(prop);
				}
			}
		}
		String crossValidation = extractCrossValidationMessage(errors, clazz);
		if (!StringUtils.isBlank(crossValidation)) {
			result.setCrossValidationMessage(crossValidation);
		}
		return result;
	}

	<T> String extractCrossValidationMessage(Set<ConstraintViolation<T>> errors, Class<T> clazz) {
		Iterator<ConstraintViolation<T>> iter = errors.iterator();
		Set<String> alle = new HashSet<>();
		while (iter.hasNext()) {
			final ConstraintViolation<T> cv = iter.next();
			if (StringUtils.isBlank(cv.getPropertyPath().toString())) {
				String message = cv.getMessage();
				alle.add(message);
			}
		}
		return ValidationStringUtils.collectionToString(alle, ",");
	}

	@SuppressWarnings("rawtypes")
	private <T> Set<String> fieldNames(Class<T> clazz) {
		Set<String> result = new HashSet<>();
		Class superClazz = clazz;
		while (superClazz != null) {
			Field[] fields = superClazz.getDeclaredFields();
			for (Field f : fields) {
				result.add(f.getName());
			}
			superClazz = superClazz.getSuperclass();
		}
		return result;
	}
}
