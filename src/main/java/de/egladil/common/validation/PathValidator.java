//=====================================================
// Projekt: de.egladil.mkv.adminservice
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.common.validation;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.PosixFilePermission;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * PathValidator prüft für einen absoluten Pfad, ob er auf ein reguläres Verzeichnis oder eine reguläre Datei zeigt und
 * ob diese für den Aufrufer lesbar ist.
 */
public class PathValidator {

	private static final Logger LOG = LoggerFactory.getLogger(PathValidator.class);

	/**
	 * Prüft ob Datei oder Verzeichnis existiert und Schreib- und Leserechte hat (u: rw). Dies ist dann der Fall, wenn
	 * keine Exception geworfen wird.
	 *
	 * @param absPath String absoluter Pfad auf der Platte. Darf nicht null sein.
	 * @throws NullPointerException wenn absPath null ist
	 * @throws IOException in allen anderen Fällen.
	 */
	public void validate(final String absPath, final IPathValidationStrategie validationStrategy) throws IOException {
		if (absPath == null) {
			throw new NullPointerException("Parameter absPath");
		}
		final Path path = Paths.get(absPath);
		validationStrategy.checkIsRegular(path);
		LOG.debug("ValidationStrategie hatte nix zu meckern");
		final Set<PosixFilePermission> permissions = Files.getPosixFilePermissions(path, new LinkOption[] { LinkOption.NOFOLLOW_LINKS });
		if (!hatLeserechte(permissions)) {
			LOG.debug("keine Leserechte");
			throw new IOException("Datei " + absPath + " kann nicht gelesen werden");
		}
		if (!hatSchreibrechte(permissions)) {
			LOG.debug("keine Schreibrechte");
			throw new IOException("Datei " + absPath + " kann nicht geändert werden");
		}
	}

	private boolean hatLeserechte(final Set<PosixFilePermission> permissions) {
		if (permissions.contains(PosixFilePermission.OWNER_READ) || permissions.contains(PosixFilePermission.GROUP_READ)) {
			return true;
		}
		return false;
	}

	private boolean hatSchreibrechte(final Set<PosixFilePermission> permissions) {
		if (permissions.contains(PosixFilePermission.OWNER_WRITE) || permissions.contains(PosixFilePermission.GROUP_WRITE)) {
			return true;
		}
		return false;
	}
}
