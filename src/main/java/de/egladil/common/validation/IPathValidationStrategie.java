//=====================================================
// Projekt: de.egladil.mkv.auswertungen
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.common.validation;

import java.io.IOException;
import java.nio.file.Path;

/**
 * IPathValidationStrategie
 */
public interface IPathValidationStrategie {

	/**
	 * Prüft, ob es sich bei dem gegebenen Path je nach Strategie um eine Datei oder ein Verzeichnis handelt.
	 *
	 * @param absPath
	 * @param path
	 * @throws IOException
	 */
	void checkIsRegular(Path path) throws IOException;

}
