//=====================================================
// Projekt: de.egladil.bv.aas
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.common.validation;

import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Kann aufgerufen werden, um ein Objekt zum gewünschten Zeitpunkt zu validieren.<br>
 * <br>
 * Verzichten der Einfachheit halber auf AOP.
 */
public class EgladilValidationDelegate {

	private static final Logger LOG = LoggerFactory.getLogger(EgladilValidationDelegate.class);

	private final Validator validator;

	/**
	 * Erzeugt eine Instanz von ValidationDelegate
	 */
	public EgladilValidationDelegate() {
		ValidatorFactory validatorFactory = Validation.buildDefaultValidatorFactory();
		validator = validatorFactory.getValidator();
	}

	/**
	 *
	 * Validiert und wirft im Fehlerfall eine EgladilValidationException.
	 *
	 * @param object
	 * @throws ConstraintViolationException
	 */
	public <T> void check(T object) throws ConstraintViolationException, IllegalArgumentException {
		if (object == null) {
			throw new IllegalArgumentException("zu validierendes object darf nicht null sein");
		}
		Set<ConstraintViolation<T>> errors = validator.validate(object);
		if (!errors.isEmpty()) {
			new ValidationUtils().debugConstraintViolation(errors, LOG);
			throw new ConstraintViolationException(errors);
		}
	}
}
