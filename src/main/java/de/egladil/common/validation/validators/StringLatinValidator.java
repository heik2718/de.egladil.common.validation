//=====================================================
// Projekt: de.egladil.persistence.tools
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.common.validation.validators;

import de.egladil.common.validation.IStringLatinConstants;
import de.egladil.common.validation.annotations.StringLatin;

/**
 * WhitelistValidator
 */
public class StringLatinValidator extends AbstractWhitelistValidator<StringLatin, String> {

	@Override
	protected String getWhitelist() {
		return IStringLatinConstants.NAME_WHITELIST_REGEXP;
	}

}
