//=====================================================
// Project: de.egladil.common.validation
// (c) Heike Winkelvoß
//=====================================================
package de.egladil.common.validation.validators;

import de.egladil.common.validation.annotations.JWTString;

/**
* JWTStringValidator
*/
public class JWTStringValidator extends AbstractWhitelistValidator<JWTString, String> {

	@Override
	protected String getWhitelist() {

		return "[a-zA-Z0-9-_]+?.[a-zA-Z0-9-_]+?.([a-zA-Z0-9-_]+)[/a-zA-Z0-9-_]+?$";
	}

}
