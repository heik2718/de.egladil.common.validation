//=====================================================
// Projekt: de.egladil.persistence.tools
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.common.validation.validators;

import de.egladil.common.validation.annotations.ClientId;

/**
 * ValidAnwendungValidator
 */
public class ClientIdValidator extends AbstractWhitelistValidator<ClientId, String> {

	private static final String REGEXP = "[a-zA-Z0-9+=]*";

	@Override
	protected String getWhitelist() {

		return REGEXP;
	}

}
