//=====================================================
// Projekt: de.egladil.persistence.tools
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.common.validation.validators;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import de.egladil.common.validation.annotations.Honeypot;

/**
 * HoneypotValidator
 */
public class HoneypotValidator implements ConstraintValidator<Honeypot, String> {

	@Override
	public void initialize(Honeypot constraintAnnotation) {
		// nix zu tun
	}

	@Override
	public boolean isValid(String value, ConstraintValidatorContext context) {
		if (value == null){
			return true;
		}
		return value.isEmpty();
	}
}
