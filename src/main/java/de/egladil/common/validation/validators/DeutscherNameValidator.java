//=====================================================
// Projekt: de.egladil.common.validation
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.common.validation.validators;

import de.egladil.common.validation.annotations.DeutscherName;

/**
 * DeutscherNameValidator
 */
public class DeutscherNameValidator extends AbstractWhitelistValidator<DeutscherName, String> {

	private static final String REGEXP = "[\\w äöüßÄÖÜ\\-@&,.()\"]*";

	/**
	 * @see de.egladil.common.validation.validators.AbstractWhitelistValidator#getWhitelist()
	 */
	@Override
	protected String getWhitelist() {
		return REGEXP;
	}

}
