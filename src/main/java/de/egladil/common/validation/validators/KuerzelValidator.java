//=====================================================
// Projekt: de.egladil.persistence.tools
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.common.validation.validators;

import de.egladil.common.validation.annotations.Kuerzel;

/**
 * KuerzelValidator. Es gibt keine Längenbeschränkunguen und keine Not-Null-Beschränkungen. Diese müssen als zusätzliche
 * Annotationen gesetzt werden!
 */
public class KuerzelValidator extends AbstractWhitelistValidator<Kuerzel, String> {

	private static final String REGEXP = "[A-Z0-9\\,]*";

	/**
	 * @see de.egladil.common.validation.validators.AbstractWhitelistValidator#getWhitelist()
	 */
	@Override
	protected String getWhitelist() {
		return REGEXP;
	}
}
