//=====================================================
// Projekt: de.egladil.common.validation
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.common.validation.validators;

import de.egladil.common.validation.annotations.LandKuerzel;

/**
 * LandKuerzelValidator
 */
public class LandKuerzelValidator extends AbstractWhitelistValidator<LandKuerzel, String> {

	private static final String REGEXP = "[A-Z\\-\\,]*";

	/**
	 * @see de.egladil.common.validation.validators.AbstractWhitelistValidator#getWhitelist()
	 */
	@Override
	protected String getWhitelist() {
		return REGEXP;
	}
}
