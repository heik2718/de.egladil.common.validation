//=====================================================
// Projekt: de.egladil.common.validation
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.common.validation.validators;

import de.egladil.common.validation.annotations.UuidString;

/**
 * KuerzelValidator
 */
public class UuidStringValidator extends AbstractWhitelistValidator<UuidString, String> {

	private static final String REGEXP = "[a-zA-Z0-9.\\-]*";

	@Override
	protected String getWhitelist() {
		return REGEXP;
	}
}
