//=====================================================
// Projekt: de.egladil.persistence.tools
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.common.validation.validators;

import de.egladil.common.validation.annotations.Dateiname;

/**
 * DateinameValidator buchstaben ohne Umlaute, ziffern, unterstrich, punkt
 */
public class DateinameValidator extends AbstractWhitelistValidator<Dateiname, String> {

	private static final String REGEXP = "[a-zA-Z0-9\\.\\_-]*";

	@Override
	protected String getWhitelist() {
		return REGEXP;
	}
}
