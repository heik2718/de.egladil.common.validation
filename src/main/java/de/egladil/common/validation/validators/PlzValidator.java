//=====================================================
// Projekt: de.egladil.persistence.tools
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.common.validation.validators;

import de.egladil.common.validation.annotations.Plz;

/**
 * PlzValidator
 *
 * [\w\-/]*
 */
public class PlzValidator extends AbstractWhitelistValidator<Plz, String> {

	/** [\w\-/]*  */
	private static final String REGEXP = "[\\w\\-/]*";

	/**
	 * @see de.egladil.common.validation.validators.AbstractWhitelistValidator#getWhitelist()
	 */
	@Override
	protected String getWhitelist() {
		return REGEXP;
	}
}
