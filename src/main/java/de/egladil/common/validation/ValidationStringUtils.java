//=====================================================
// Projekt: de.egladil.bv.gottapp
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.common.validation;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;

import com.google.common.base.Splitter;

public final class ValidationStringUtils {

	/**
	 * Wandelt eine Collection von Strings in einen durch separationChars getrennten Einzelstring ohne lLeerzeichen
	 * um.<br>
	 * <br>
	 * Wenn strings null oder leer ist, wird "" zurückgegeben.<br>
	 * Wenn strings nur ein Element enthält, wird dieses zurückgegeben.<br>
	 * Wenn separationChars null ist oder Länge 0 hat, werden alle Elemente aneinandergeklatscht.
	 *
	 * @param strings Collection.
	 * @param separationChars
	 * @return String nie null.
	 */
	public static String collectionToString(Collection<String> strings, String separationChars) {
		if ((strings == null) || (strings.isEmpty())) {
			return "";
		}
		List<String> copy = new ArrayList<>();
		Iterator<String> iterator = strings.iterator();
		while (iterator.hasNext()) {
			copy.add(iterator.next());
		}
		return listToString(copy, separationChars);
	}

	private static String listToString(List<String> strings, String separationChars) {
		StringBuffer sb = new StringBuffer();
		final String separator = (separationChars == null || separationChars.isEmpty()) ? "" : separationChars;

		for (int i = 0; i < strings.size() - 1; i++) {
			sb.append(strings.get(i));
			sb.append(separator);
		}
		sb.append(strings.get(strings.size() - 1));
		return sb.toString();
	}

	/**
	 * Nimmt einen durch separationChars getrennten String, entfernt alle Teile, die mehrfach vorkommen und gibt den
	 * resultierenden String mit dem gleichen separationChar getrennt als String zurück.
	 *
	 * @param string String falls null, dann ""
	 * @param separationChars
	 * @return String nie null
	 */
	public static String toStringOhneDubletten(String string, String separationChars) {
		if (StringUtils.isBlank(string)) {
			return "";
		}
		Set<String> token = new HashSet<>();

		List<String> alle = Splitter.on(separationChars).trimResults().splitToList(string);
		for (String t : alle) {
			token.add(t);
		}
		return collectionToString(token, separationChars);
	}
}
